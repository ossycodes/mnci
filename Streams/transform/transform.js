/**
 * Let's imagine a program that helps convert ASCII (American Standard Code for Information Interchange) codes into ASCII characters, receiving input from stdin
 * You type in an ASCII code, and the program responds with the alphanumeric character corresponding to that code.
 * Here we can simply pipe our input to a Transform stream, then pipe its output back
 * to stdout.
 */

// const stream = require("stream");
// let converter = new stream.Transform();

// converter._transform = function (num, encoding, cb) {
//     this.push(String.fromCharCode(new Number(num)) + "\n");
//     cb();
// }

// process.stdin.pipe(converter).pipe(process.stdout);

/**
 * Example 2 ES6
 */
// const { Transform } = require("stream");

// class ReplaceText extends Transform {
//     constructor(char) {
//         super();
//         this.replaceChar = char;
//     }

//     _transform(chunk, encoding, callback) {
//         const transformChunk = chunk.toString().replace(/[a-z]|[A-z]|[0-9]/g, this.replaceChar);
//         this.push(transformChunk);
//         callback();
//     }

//     _flush(callback) {
//         this.push("more stuffs can still be passed with _flush");
//     }
// }

// const xStream = new ReplaceText("X");

// process.stdin.pipe(xStream).pipe(process.stdout);

/**
 * 
 */
// const { Transform } = require("stream");

// const upperCaseTr = new Transform({
//     transform(chunk, encoding, callback) {
//         this.push(chunk.toString().toUpperCase());
//         callback();
//     }
// });

// process.stdin.pipe(upperCaseTr).pipe(process.stdout);

/**
 * several I/O modules are implemented as Transform Streams.
 * eg zlib 
 * in this example we read a filename from the argument
 * create a readStream from that fileName, pipe that into
 * the transform stream from gzip zlib, and pipe that
 * into a writeable stream on the filesystem, using the
 * same filename but with a .gz extension
 */
// const fs = require("fs");
// const zlib = require("zlib");
// const file = process.argv[2];

// fs.createReadStream(file)
//     .pipe(zlib.createGzip())
//     .pipe(fs.createWriteStream(file + '.gz'));


// const { createGzip } = require("zlib");
// const {
//     createReadStream,
//     createWriteStream
// } = require("fs");

// const gzip = createGzip();
// const source = createReadStream(process.argv[2]);
// const destination = createWriteStream(`${process.argv[2]}.gz`);

// source.pipe(gzip).pipe(destination);

/**
 * stream.pipeline(source[, ...transforms], destination, callback);
 * this module method (pipeline) 
 * is a module method to pipe between streams forwarding errors and properly
 * cleaning up and provide a callback when everything is complete.
 * 
 */
// const { createGzip } = require("zlib");
// const { pipeline } = require("stream");
// const file = process.argv[2];

// const {
//     createReadStream,
//     createWriteStream
// } = require("fs");

// const gzip = createGzip();
// const source = createReadStream(file);
// const destination = createWriteStream(`${file}.gz`);

// pipeline(source, gzip, destination, (err) => {
//     if (err) {
//         console.error('An error occured:', err);
//     }
//     process.exitCode = 1;
// });

// Or, Promisified
const { promisify } = require("util");
const { pipeline } = require("stream");
const { createGzip } = require("zlib");
const pipe = promisify(pipeline);
const file = process.argv[2];

const {
    createReadStream,
    createWriteStream
} = require("fs");

async function do_gzip(input, output) {
    const gzip = createGzip();
    const source = createReadStream(input);
    const destination = createWriteStream(output);
    await pipe(source, gzip, destination);
}

do_gzip(file, `${file}.gz`)
    .catch((err) => {
        console.error("An error occured:", err);
        process.exitCode = 1;
    });
