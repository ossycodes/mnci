/**
 * A writeable stream is responsible for accepting some value
 * (a stream of bytes, a string) and writing that data to a destination
 * (one should think of a writable stream as a data taget)
 */
// const stream = require("stream");
// const { callbackify } = require("util");
// let writeable = new stream.Writable({
    // highWaterMark: 160000,
//     decodeStrings: true
// });

/**
 * highWaterMark: the maximum number of bytes the stream's buffer will
 * accept prior to returning false on writes. Default is 16kb
 * decodeStrings: Whether to convert strings into buffers before writing
 * Default is true
 */
/**
 * as with Readable streams, custom writeable stream implementations 
 * must implement a _write handler which will be passed the arguments sent to the
 * write method of instances
 */

/**
 * implementation
 */
// writeable._write = (chunk, encoding, callback) => {
//     console.log(chunk.toString());
//     callback();
// }

/**
 * consumer
 */
// let written = writeable.write(Buffer.alloc(32, "A"));
// writeable.end();

// console.log(written);

/**
 * there are two key things to note here
 * First, our _write implementation fires the callback function
 * immediately after writing a callback that is always present, regardless
 * of whether the instance write method is passed a callback directly. This
 * call is important for indicating the status of the write attempt, whether a
 * failure(error or a success)
 * Second, the call to write returned true. This indicates that the internal buffer of the
 * Writeable implementation has been emptied after executing the requested write.
 * What i we sent a very large amount of data, enough to exceed the dafult size of the 
 * internal buffer?
 * Modifying the previous example, the following would return false
 */

/**
 * consumer
 */
// let written = writeable.write(Buffer.alloc(16394, "A"));
// writeable.end();
// console.log(written);

/** example */
// const { createWriteStream } = require("fs");
// const writeStream = createWriteStream('./file.txt')

// process.stdin.pipe(writeStream).on("error", console.error);

/**
 * The reason this write returns false is that it has reached the highWaterMark
 * option -- default value of 16KB. if we changed this value to 16383 write would again
 * return true (or one could simply increase its value)
 * What should you do when write returns false ?
 * You should certainly not continue to send data!
 * Node's stream implementation will emit a drain event whenever it is safe to write again
 * when write returns false listen for the drain event before sending more data.
 */

/**
 * putting together all this, let's create a Writeable stream with a highWaterMark
 * of value of 10 bytes. We'il then set up a simulation where we push a string of data
 * to stdout larger than the highWaterMark somem number of times.
 * We catch buffer overflows and wait for the drain event to fire prior to
 * sending more data
 */
const stream = require("stream");

let writable = new stream.Writable({
    highWaterMark: 10
});

writable._write = (chunk, encoding, callback) => {
    process.stdout.write(chunk);
    callback();
}

function writeData(iterations, writer, data, encoding, cb) {

    (function write() {

        if (!iterations--) {
            return cb();
        }

        if(!writer.write(data, encoding)) {
            console.log(`<wait> highWaterMark of ${writable.writableHighWaterMark} reached`);
            
            writer.once("drain", write);
        }

    })();

}

writeData(3, writable, 'String longer than highWaterMark', 'utf8', () => {
    console.log("finished");
});

/**
 * Each time we write we check if the stream write action returned false, and if so we wait
 * for the next drain event before running our write method again.
 */