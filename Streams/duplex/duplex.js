/**
 * A duplex stream is both readable and writeable, For instance, a TCP server, it is useful when we want to describe an entity that
 * is both a data source and a data destnation, such as network sockets, Duplex streams inherit the methods of both stream.Readable and
 * stream.Writeable, so this is nothing new to us. this means we can read() or write() data, or listen for both the readable and drain events
 * 
 */

// const { Duplex } = require("stream");
// const { read } = require("fs");

// const inputStream = new Duplex({

//     write(chunk, encoding, callback) {
//         console.log(chunk.toString());
//         callback();
//     },

//     read(size) {
//         if (this.currentCharCode > 90) {
//             this.push(null);
//             return;
//         }
//         this.push(String.fromCharCode(this.currentCharCode++));
//     }

// });

// inputStream.currentCharCode = 65;

// process.stdin.pipe(inputStream).pipe(process.stdout);

const { Duplex, PassThrough } = require("stream");

class Throttle extends Duplex {
    constructor(ms) {
        super();
        this.delay = ms;
    }

    _write(chunk, encoding, callback) {
        console.log("thro");
        this.push(chunk);
        setTimeout(callback, this.delay);
    }

    _read() {

    }

    /**
     * no more data from the read stream
     */
    _final() {
        this.push(null);
    }
}

const report = new PassThrough();
const throttle = new Throttle(100);

let total = 0;
report.on("data", (chunk) => {
    total += chunk.length;
    console.log("reporting bytes", total);
});

process.stdin
    .pipe(throttle)
    .pipe(report)
    .pipe(process.stdout);
