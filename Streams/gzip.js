const fs = require("fs");
const zlib = require("zlib");

const file = process.argv[2];


/**
 * 1) USING BUFFER
 * almost all the asynchronous APIS work using buffer mode,
 * which for an input operation like fs.readFile, buffer mode
 * causes all the data coming from a resource to be collected
 * into a buffer, it is then passed to a callback as soon as the
 * entire resource is read
 */
// fs.readFile(file, (err, buffer) => {
//     zlib.gzip(buffer, (err, buffer) => {
//         fs.writeFile(file + ".gz", buffer, (err) => {
//             console.log("File successfully compressed");
//         });
//     });
// });


/**
 * 2) USING STREAMS
 */
fs.createReadStream(file)
    .pipe(zlib.createGzip())
    .pipe(fs.createWriteStream(file + ".gz"))
    .on("finish", () => {
        console.log("File successfully compressed")
    });
