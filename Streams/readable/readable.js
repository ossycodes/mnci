/**
 * stream producing data another process may have interest in
 * are normally implemented using a Readable stream.
 */

//  const stream = require("stream");

//  let readable = new stream.Readable({
//     encoding: "utf-8",
//     highWaterMark: 1600,
//     objectMode: true
//  });

/**
 * Readable is exposed as a base class, which can be initialized through three options
 * 1) encoding: Decode buffers into the specified encoding, defaulting to UTF-8
 * 2) highWaterMark: Number of bytes to keep in the internal buffer before ceasing to read to
 *    read from the data source. the default is 16KB
 * 3) objectMode: Tell the stream to behave as a stream of objects instead of a stream of bytes,
 *    such as a stream of JSON objects instead of the bytes in a file. Default is false.
 * 
 */


/**
 * in the following example, we create a mock feed object whose instances will inherit
 * the Readable stream interface. Our implementation need only implement the avstract _read method
 * of Readable, which will push data to a consumer until there is nothing more to push, at which 
 * point it triggers the Readable stream to emit an end event by pushing a null value
 */

// const stream = require("stream");

// let Feed = function (channel) {
//     let readable = new stream.Readable({});
//     let news = [
//         "Big win",
//         "Stocks Down",
//         "Actor sad!"
//     ];

//     readable._read = () => {
//         if (news.length) {
//             return readable.push(news.shift() + "\n");
//         }
//         readable.push(null);
//     };
//     return readable;
// }

/**
 * now that we have an implentation, a consumer might want to instantiate the stream and listen
 * for stream events. two key events are "readable" and "end"
 * The readable event is emitted as long as data is being pushed to the stream. It alerts the
 * consumer to check for  new data via the read method of readable.
 * The end event will be emitted whenever a null value is passed to the push method of our 
 * Readable implementation
 */
/**
 * here we see a consumer using these methods to display new stream data, providing a notification
 * when the stream as stopped sending data
 */

// let feed = new Feed();

/**
 * you can use either pipe or
 */
// feed.pipe(process.stdout);

/**
 * you can use events, but avoid mixing them
 */

// feed.on("readable", () => {
/**
 *in paused mode, we have  to use the stream.read()
 *to read from the stream
 */
//     let data = feed.read();
//     data && process.stdout.write(data);
// });

//another way
/**
 *in flowing mode  we  have to use EventEmitter ,
 *listen to events to consume data 
 * 
 */
// feed.on("data", (chunk) => {
//     process.stdout.write(chunk.toString());
// });

// feed.on("end", () => {
//     console.log("No more news");
// });

// const { Readable } = require("stream");

// const inStream = new Readable();

// inStream.push("ABCDEF");
// inStream.push(null);

// inStream.on("data", (chunk) => {
//     process.stdout.write(chunk);
// });

// inStream.on("readable", () => {
//     let data = inStream.read();
//     data && process.stdout.write(data);
// });

// inStream.pipe(process.stdout);

// const { Writable } = require("stream");

// const outStream = new Writable({
//     write(chunk, encoding, callback) {
//         console.log(chunk.toString());
//         callback();
//     }
// });

// process.stdin.pipe(outStream);

/** ES6 WAY */

const { Readable } = require("stream");

const peaks = [
    "Tallac",
    "Ralston",
    "Rubicon",
    "Twin Peaks",
    "Castle Peak",
    "Rose",
    "Freel Peak"
];

/**
 * implementation
 */

class StreamFromArray extends Readable {

    constructor(array) {
        /**
         * we will read our array bit by bit as buffers
         * that means this stream is operating in binary mode
         * streams have two modes, binary and object mode
         * when they are in binary mode we can read as a binary
         * or as a string, inorder to read as a string you just
         * have to set encoding as UTF-8
         */
        // super(); 
        super({
            encoding: "utf-8"
        }); 
        this.array = array;
        this.index = 0;
    }

    _read() {
        if (this.index <= this.array.length) {
            const chunk = this.array[this.index];
            this.push(chunk);
            this.index += 1;
        } else {
            this.push(null);
        }
    }
}

/**
 * consumer
 */
const peakStream = new StreamFromArray(peaks);

peakStream.on("data", (chunk) => {
    console.log(chunk);
});

peakStream.on("end", () => {
    console.log("done");
});
