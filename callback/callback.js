/**
 * see closure.js explanation first
 * callbacks are functions that are invoked to propate the result of an operation, (callback is a function passed as an argument
 * to another function and is invoked with the result when the operation completes) another definitions is A callback
 * is a block of instruction wrapped in a function to be called when an asynchronous call has completed.
 *  and this is exactly what we need when dealing with asynchronous operations. They do replace the use of the return
 * instruction that always executes synchronously. JavaScript is a great language to represent callbacks, because functions are
 * first class objects and can be easily be assigned to variables, passed as arguments, returend from another function invocation
 * or stored in data structures. Another Ideal construct for implementing callback is closures.
 * With closures, we can in fact reference the environment in which a function was created, we can always maintain the context in
 * which the asynchronous operation was requested, no matter when or where it's callback is invoked.
 */

/**
 * Continuation-Passing Style CPS
 * In JavaScript , a callback is a function that is passed as an argument to another function and is invoked with the result
 * when the operation completes. In functional programming, this way of propagating result is called `continuous-passing style CPS`
 * it is a general concept, and it is not always associated with asynchronous operations. In fact; it simply indicates that a result is
 * propagated by passing it to another function (the callback), instead of directly returning it directly to the caller
 */

/**
 * Synchronous Continuation-Passing Style
 */
// function add(a, b) {
//     return a + b;
// }
/**
 * there is nothing special here; the result is passed back to the caller using the `return` instruction; this also called `direct style` and
 * it represents the most common way of returning a result in `synchronous programming`. The equivalent continuation-passing style of the pre
 * ceeding function would be as follows:
 */
// function add(a, b, callback) {
//     callback(a + b);
// }

// console.log("Before");
// add(1, 2, result => {
//     console.log(result);
// });
// console.log("after");

/**
 * since the add() is `synchronous CPS`,  the previous code will trivally print the following:
 * before
 * Result: 3
 * after
/**
 * this add() function is a `synchronous CPS function`, which means it will return a value only when the callback completes it's execution
 */

// function hideString(str, callback) {
//     callback(str.replace(/[a-zA-Z]/g, "X"));
// }

// hideString("Hello World", (res) => {
//     console.log(res);
// });

/**
 * Asynchronous Continuation-Passing style
 */
// function additionAsync(a, b, callback) {
//     setTimeout(() => callback(a + b));
// }
/**
 * we used the setTimeout() to simulate an asynchronous invocation of the callback. Now let's try to use additionAsync and see how
 * the oder of the operation changes
 */

// console.log("before");
// additionAsync(1, 2, result => console.log('Result:' + result));
// console.log("after");

/**
 * the preceding code will print the following output
 * beofe
 * after
 * result: 3
 */
/**
 * since setTimeout() triggers an asynchronous operation, it will not wait for the callback to be executed, but instead, it returns imm
 * ediately, giving the control back, as it gives the control back to the event loop as soon asynchronous request is sent, thus allowing
 * a new event from the queue to be processed.
 * Whwn the asynchronous operation completes, the execution is then resumed starting from the callback provided to the asynchronous function
 * that caused the unwinding. Thanks to closures, it is trival to maintain the context of the `caller` of the asynchronous function, even if
 * the callback is invoked at a different point in time from a different location.
 */
