/**
 * util.callbackify(original), takes an async function,
 * or a function taht returns a Promise and returns a function
 * following the error-first callback style
 */

const util = require("util");

async function fn() {
    return "hello world";
}

const callbackFunction = util.callbackify(fn);

callbackFunction((err, ret) => {
    if (err) {
        throw err;
    }
    console.log(ret);
});


const asyncFunction = util.promisify(callbackFunction);

(async function x() {
     res = await asyncFunction();
    console.log(res);
}());