/**
 * when I invoke greet I get a value back, but instead of a string or number,
 * I am going to get a function, that I can then invoke again 
 * (BECAUSE FUNCTIONS ARE OBJECTS I CAN JUST RETURN IT AS A VALUE)
 */
function greet(whattosay) {

    return function (name) {
        console.log(whattosay + " " + name);
    }

}

// greet("HI")("Tony");
const sayHi = greet("Hi");
sayHi("Tony");

/**
 * the above works, so lets stop and think about it for a minute.
 * How does the sayHi function still know the `whattosay` variable? because the `whattosay` variable was created here and then that function
 * is done, it's over, it;s completed it's execution context and popped off the execution stack and yet when I invoke sayHi() it still as the proper
 * value of `whattosay` , so how's that possible? It's possible because of closures.
 * when sayHi("Tony") is invoked, and then Javascript sees the `whattosay` variable, it goes up to scope chain there is an outer lexical environment re
 * ference (it goes outside to the next point where the function was created to look for that variable, since it could not find it inside the function itself)
 * and even if the execution context of that function `greet` was popped of the stack the sayHi execution context still has a reference to the variables and to
 * the memory space of it's outer environment.
 * In this way we say the execution context of sayHi has `closed in it;s outer variables` even though those execution contexts are gone. AND SO THIS PHENOMENOM
 * OF CLOSING IN ALL THE VARIABLES IT'S SIPPOSED TO HAVE ACCESS TO IS CALLED A `CLOSURE`, hence CLOSURE is;nt something you tell the javaScript to do, closures are
 * simply a feature of the JavaScript programming language, THEY JUST HAPPEN, it does not matter when we invoke a function, we don;t have to worry if it;s outer env
 * ironments are still running, the javaScript engine will always make sure that whatever function I am running will have access to the variables it's supposed to have
 * access to.
 */